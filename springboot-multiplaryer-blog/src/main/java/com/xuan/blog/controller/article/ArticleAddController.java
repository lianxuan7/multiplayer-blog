package com.xuan.blog.controller.article;

import com.xuan.blog.service.article.ArticleAddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/article")
public class ArticleAddController {

    @Autowired
    ArticleAddService articleAddService;

    @RequestMapping("/add")
    public Boolean articleAdd(@RequestBody HashMap hashMap){

        return articleAddService.articleAdd(hashMap);
    }

    @RequestMapping("/addDraft")
    public Boolean articleAddDraft(@RequestBody HashMap hashMap){

        return articleAddService.articleAddDraft(hashMap);
    }
    @RequestMapping("/addDiscuss")
    public Boolean articleAddDiscuss(@RequestBody HashMap hashMap){
        System.out.println(hashMap);
        return articleAddService.articleAddDiscuss(hashMap);
    }

    @RequestMapping("/delDraft")
    public Boolean articleDelDraft(@RequestBody String articleId){

        return articleAddService.articleDelDraft(articleId);
    }

    @RequestMapping("/updateArticle")
    public Boolean articleUpdate(@RequestBody HashMap hashMap){

        return articleAddService.articleUpdate(hashMap);
    }
}
