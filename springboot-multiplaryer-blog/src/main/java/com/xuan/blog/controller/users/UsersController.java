package com.xuan.blog.controller.users;

import com.xuan.blog.service.users.UsersService;
import com.xuan.blog.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    UsersService usersService;



    @RequestMapping("/headpic")
    public String usersHeadpic(@RequestHeader("token") String token){
        Integer userId = JwtUtils.verifyToken(token);
        return usersService.usersHeadpic(userId);
    }

    @RequestMapping("/homeAddress")
    public String userHomeAddress(@RequestBody String userId){
        return usersService.usersHomeAddress(Integer.parseInt(userId));
    }

    @RequestMapping("/follow")
    public Boolean userFollow(@RequestBody HashMap hashMap){
        return usersService.userFollow(hashMap);
    }


}
