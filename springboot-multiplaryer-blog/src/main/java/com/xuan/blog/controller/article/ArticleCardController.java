package com.xuan.blog.controller.article;

import com.xuan.blog.pojo.article.ArticleCard;
import com.xuan.blog.service.article.ArticleCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleCardController {

    @Autowired
    ArticleCardService articleCardService;

    @RequestMapping("/card")
    public List<ArticleCard> selectAllUsers(@RequestBody String userId){
        return articleCardService.selectAllArticle(userId);
    }

    @RequestMapping("/card/user")
    public List<ArticleCard> selectUsersArticle(@RequestBody HashMap hashMap){
        return articleCardService.selectUsersArticle(hashMap);
    }

    @RequestMapping("/card/draft")
    public List<HashMap> selectUsersDraftArticle(@RequestBody String userId){
        return articleCardService.selectUsersDraftArticle(userId);
    }

    @RequestMapping("/search")
    public List<ArticleCard> searchArticle(@RequestBody HashMap hashMap){
        return articleCardService.searchArticle(hashMap);
    }
}
