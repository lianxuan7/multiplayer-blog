package com.xuan.blog.controller.article;

import com.xuan.blog.service.article.ArticleSocialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/article")
public class ArticleSocialController {

    @Autowired
    ArticleSocialService articleSocialService;


    @RequestMapping("/userLikeAdd")
    public Boolean articleUserLikeAdd(@RequestBody HashMap hashMap){
        // 用户id 文章id
        return articleSocialService.articleUserLikeAdd(hashMap);
    }
}
