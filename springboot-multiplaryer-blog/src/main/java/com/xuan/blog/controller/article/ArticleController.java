package com.xuan.blog.controller.article;

import com.xuan.blog.service.article.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @RequestMapping("/content")
    public HashMap articleContent(@RequestBody HashMap hashMap){
        return articleService.articleContent(hashMap);
    }

    @RequestMapping("/discuss")
    public List<HashMap> articleDiscuss(@RequestBody HashMap hashMap){
        return articleService.articleDiscuss(hashMap);
    }

    @RequestMapping("/draftContent")
    public HashMap draftContent(@RequestBody String articleId){
        return articleService.articleDraftContent(articleId);
    }
}
