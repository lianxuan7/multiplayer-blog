package com.xuan.blog.controller.users;

import com.xuan.blog.pojo.users.UsersHome;
import com.xuan.blog.service.users.UsersHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/users")
public class UsersHomeController {

    @Autowired
    UsersHomeService usersHomeService;

    @RequestMapping("/homeMeta")
    public UsersHome userHomeAddress(@RequestBody HashMap hashMap){
        return usersHomeService.usersHome(hashMap.get("homeAddress").toString(),Integer.parseInt(hashMap.get("userId").toString()));
    }

    @RequestMapping("/homeSettingsMeta")
    public HashMap userHomeSettings(@RequestBody String userId){
        return usersHomeService.usersHomeSettings(Integer.parseInt(userId));
    }

    @RequestMapping("/homeSettingsUpdate")
    public Boolean userHomeUpdate(@RequestBody HashMap hashMap){
        return usersHomeService.userHomeUpdate(hashMap);
    }
}
