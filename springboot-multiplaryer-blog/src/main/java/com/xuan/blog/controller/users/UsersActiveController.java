package com.xuan.blog.controller.users;

import com.xuan.blog.pojo.users.Users;
import com.xuan.blog.service.users.UsersActiveService;
import com.xuan.blog.utils.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 用户登录注册
@RestController
@RequestMapping("/users")
public class UsersActiveController {

    @Autowired
    UsersActiveService usersActiveService;

    @RequestMapping("/userlogin")
    public ResultVO userLogin(@RequestBody Users users){
        return usersActiveService.userLogin(users);
    }
}
