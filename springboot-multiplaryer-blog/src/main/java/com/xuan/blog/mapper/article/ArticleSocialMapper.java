package com.xuan.blog.mapper.article;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
@Mapper
public interface ArticleSocialMapper {

    Integer articleUserIsLike(HashMap hashMap);

    Integer articleUserModifyLike(String status,Integer artPraiseId);

    Integer articleUserLike(HashMap hashMap);

    Integer articleUserLikeAdd(Integer articleId);

    Integer articleUserLikeSub(Integer articleId);
}
