package com.xuan.blog.mapper.article;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
@Mapper
public interface ArticleAddMapper {

    Integer articleAdd(HashMap hashMap);

    Integer articleAddDraft(HashMap hashMap);

    Integer articleAddMd(HashMap hashMap);

    Integer articleAddDiscuss(HashMap hashMap);

    Integer articlePlusComment(String articleId);

    Integer articleSocialAdd(Integer userId);

    Integer articleUpdate(HashMap hashMap);

    Integer articleDetailUpdate(HashMap hashMap);

    Integer articleDelDraft(String articleId);
}
