package com.xuan.blog.mapper.users;


import com.xuan.blog.pojo.users.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UsersActiveMapper {

    Users userLogin(Users users);
}
