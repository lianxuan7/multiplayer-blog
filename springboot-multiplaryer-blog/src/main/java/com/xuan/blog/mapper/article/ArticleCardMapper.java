package com.xuan.blog.mapper.article;


import com.xuan.blog.pojo.article.ArticleCard;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface ArticleCardMapper {

    List<ArticleCard> selectAllArticle(String userId);

    List<ArticleCard> selectUsersArticle(HashMap hashMap);

    List<HashMap> selectUsersDraftArticle(String userId);

    List<ArticleCard> searchArticle(HashMap hashMap);
}
