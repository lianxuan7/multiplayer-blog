package com.xuan.blog.mapper.users;

import com.xuan.blog.pojo.users.UsersHome;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
@Mapper
public interface UsersHomeMapper {

    UsersHome usersHome(String homeAddress,Integer userId);

    HashMap usersHomeSettings(Integer userId);

    Integer userHomeUpdate(HashMap hashMap);
}
