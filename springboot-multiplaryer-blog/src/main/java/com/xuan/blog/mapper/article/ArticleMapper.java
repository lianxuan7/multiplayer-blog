package com.xuan.blog.mapper.article;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface ArticleMapper {

    HashMap articleContent(HashMap hashMap);

    List<HashMap> articleDiscuss(HashMap hashMap);

    HashMap articleDraftContent(String articleId);
}
