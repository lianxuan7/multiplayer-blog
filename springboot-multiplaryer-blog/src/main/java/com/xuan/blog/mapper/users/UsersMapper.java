package com.xuan.blog.mapper.users;


import com.xuan.blog.pojo.users.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface UsersMapper {

    List<Users> selectAllUsers();

    Users selectUsers();

    String usersHeadpic(Integer userId);

    String usersHomeAddress(Integer userId);

    Integer usersIsFollow(HashMap hashMap);

    Integer usersModifyFollow(String status,Integer userFollowId,String currentTime);

    Integer userFollow(HashMap hashMap);

    Integer userSocialAdd(Integer passiveFollowId);

    Integer userSocialSub(Integer passiveFollowId);
}
