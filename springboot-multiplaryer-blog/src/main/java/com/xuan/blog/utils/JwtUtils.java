package com.xuan.blog.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * @author Lehr
 * @create: 2020-02-04
 */
public class JwtUtils {

    private static final String KEY = "Q!2#q.a-4%/>?<:*-~";

    public static String createToken(Integer userId){
        String token = JWT.create()
                .withClaim("userId", userId)
                .sign(Algorithm.HMAC256(KEY));

        return token;
    }


    public static Integer verifyToken(String token){
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(KEY)).build();
        DecodedJWT verify = verifier.verify(token);
        return verify.getClaims().get("userId").asInt();
    }

}

