package com.xuan.blog.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationConfig implements WebMvcConfigurer {


    //设置本地资源能被访问
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/Blog/**").addResourceLocations("file:C:/Users/Administrator/Desktop/实战项目/MultiplayerBlog/springboot-multiplaryer-blog/files/");
    }

}