package com.xuan.blog.pojo.article;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@AllArgsConstructor
@Data
public class ArticleCard {

    private String nickName;    // 发表用户昵称

    private BigInteger articleId;  // 文章ID

    private String pushDate;    // 文章发表时间

    private String title;   // 文章标题

    private String articleImg;  // 文章封面 ?

    private String articleSummary;  //  文章摘要

    private Integer readCount;  // 阅读量

    private Integer likeCount;  //  点赞量

    private Integer commentCount;   // 评论数

    private String tagName; // 标签名

    private Integer artPariseId; // 当前用户是否点赞 0未点赞   其他数字为点赞
}
