package com.xuan.blog.pojo.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users {

    private BigInteger userId;  // 用户ID

    private String userName;    // 用户名

    private String passWord;    // 用户密码

    private String email;   // 邮箱

    private String nickName;    // 昵称

    private String gender;  // 性别(1:男 2:女)

    private String headpic; // 头像地址

    private String mobile_phone;    // 手机号

    private String RegisterTime;    // 注册时间 时间戳

    private String summary; // 自我简介

    private String homeAddress; // 自定义的访问路径

    private String qqNumber;    // QQ号

    private String weChat;  // 微信号

    private String blogAddress; // 自己的博客链接
}
