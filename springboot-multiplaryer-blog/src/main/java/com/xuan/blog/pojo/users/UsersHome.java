package com.xuan.blog.pojo.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersHome {
    private BigInteger userId;  // 用户Id

    private String nickName;    // 用户昵称

    private String headpic; // 用户头像地址

    private String summary; // 自我简介

    private Integer fansCount;   // 粉丝数

    private Integer followCount; // 关注数

    private Integer articleCount;    // 文章数

    private BigInteger userFollowId;    // 是否关注
}
