package com.xuan.blog.pojo.article;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@AllArgsConstructor
@Data
public class ArticleTag {

    private BigInteger artTagId;    // 标签ID

    private String tagName; // 标签名

    private String description; // 标签描述

    private String createTime;  // 创建时间
}
