package com.xuan.blog.pojo.users.article;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@AllArgsConstructor
@Data
public class Article {
    private BigInteger articleId;    //文章ID

    private String pushDate; // 发布时间

    private String modifyDate; // 最后修改日期

    private BigInteger userId;  // 发表用户id

    private String title;   // 标题

    private Integer likeCount;  // 点赞数

    private int commentCount;   // 评论数

    private int readCount;  // 浏览量

    private String topFlag; // 是否顶置 0F 1T

    private String articleSummary;  // 文章摘要

    private String articleImg;  // 文章封面地址 ?

    private BigInteger categoryId;  // 分类ID

    private BigInteger tagId;   // 标签ID
}
