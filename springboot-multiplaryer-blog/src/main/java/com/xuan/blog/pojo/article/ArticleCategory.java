package com.xuan.blog.pojo.article;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@AllArgsConstructor
@Data
public class ArticleCategory {

    private BigInteger artCategoryId;   // 分类ID

    private String categoryName;    // 分类名称

    private String description; // 分类描述

    private String createTime;  // 创建时间
}
