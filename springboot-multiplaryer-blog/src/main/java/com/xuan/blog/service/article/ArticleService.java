package com.xuan.blog.service.article;


import com.xuan.blog.mapper.article.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ArticleService implements ArticleMapper{

    @Autowired
    ArticleMapper articleMapper;


    @Override
    public HashMap articleContent(HashMap hashMap) {
        return articleMapper.articleContent(hashMap);
    }

    @Override
    public List<HashMap> articleDiscuss(HashMap hashMap) {
        return articleMapper.articleDiscuss(hashMap);
    }

    @Override
    public HashMap articleDraftContent(String articleId) {
        return articleMapper.articleDraftContent(articleId);
    }
}
