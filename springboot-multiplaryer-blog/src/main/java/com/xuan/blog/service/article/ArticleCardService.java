package com.xuan.blog.service.article;


import com.xuan.blog.mapper.article.ArticleCardMapper;
import com.xuan.blog.pojo.article.ArticleCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ArticleCardService{

    @Autowired
    ArticleCardMapper articleCardMapper;


    public List<ArticleCard> selectAllArticle(String userId) {
        return articleCardMapper.selectAllArticle(userId);
    }


    public List<ArticleCard> selectUsersArticle(HashMap hashMap) {
        return articleCardMapper.selectUsersArticle(hashMap);
    }

    public List<HashMap> selectUsersDraftArticle(String userId) {
        return articleCardMapper.selectUsersDraftArticle(userId);
    }

    public List<ArticleCard> searchArticle(HashMap hashMap) {
        return articleCardMapper.searchArticle(hashMap);
    }
}
