package com.xuan.blog.service.users;


import com.xuan.blog.mapper.users.UsersHomeMapper;
import com.xuan.blog.pojo.users.UsersHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
//public class UsersHomeService implements UsersHomeMapper {
public class UsersHomeService{

    @Autowired
    UsersHomeMapper usersHomeMapper;



    public UsersHome usersHome(String homeAddress, Integer userId) {
        return usersHomeMapper.usersHome(homeAddress,userId);
    }


    public HashMap usersHomeSettings(Integer userId) {
        return usersHomeMapper.usersHomeSettings(userId);
    }


    public boolean userHomeUpdate(HashMap hashMap) {
        Integer integer = usersHomeMapper.userHomeUpdate(hashMap);
        if (integer >= 1){
            return true;
        }

        return false;
    }
}
