package com.xuan.blog.service.article;


import com.xuan.blog.mapper.article.ArticleAddMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

@Service
public class ArticleAddService{

    @Autowired
    ArticleAddMapper articleAddMapper;


    public Boolean articleAdd(HashMap hashMap) {
        Integer integer = articleAddMapper.articleAdd(hashMap);
        if (integer >=1){
            Integer integer1 = articleAddMapper.articleAddMd(hashMap);

            articleAddMapper.articleSocialAdd(Integer.parseInt(hashMap.get("userId").toString()));
            if (integer1 >=1){
                return true;
            }
        }
        return false;
    }

    public Boolean articleAddDraft(HashMap hashMap) {
        Integer integer = articleAddMapper.articleAddDraft(hashMap);
        if (integer >=1){
            Integer integer1 = articleAddMapper.articleAddMd(hashMap);

            articleAddMapper.articleSocialAdd(Integer.parseInt(hashMap.get("userId").toString()));
            if (integer1 >=1){
                return true;
            }
        }
        return false;
    }

    @Transactional
    public Boolean articleAddDiscuss(HashMap hashMap) {
        Integer integer = articleAddMapper.articleAddDiscuss(hashMap);
        if (integer >=1){
            Integer integer1 = articleAddMapper.articlePlusComment(hashMap.get("articleId").toString());

            if (integer1 >=1){
                return true;
            }
        }
        return false;
    }

    public Boolean articleUpdate(HashMap hashMap){

        Integer integer = articleAddMapper.articleUpdate(hashMap);

        if (integer >= 1){
            Integer integer1 = articleAddMapper.articleDetailUpdate(hashMap);
            if (integer1 >= 1){
                return true;
            }
        }

        return false;
    }

    public Boolean articleDelDraft(String articleId){

        Integer integer = articleAddMapper.articleDelDraft(articleId);

        if (integer >= 1){
                return true;
        }

        return false;
    }

}
