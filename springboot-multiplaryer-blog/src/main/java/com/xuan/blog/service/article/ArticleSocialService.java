package com.xuan.blog.service.article;


import com.xuan.blog.mapper.article.ArticleSocialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ArticleSocialService{

    @Autowired
    ArticleSocialMapper articleSocialMapper;


    public Boolean articleUserLikeAdd(HashMap hashMap) {
        Integer integer = articleSocialMapper.articleUserIsLike(hashMap);
        if (integer != null){
            Integer status = articleSocialMapper.articleUserModifyLike(hashMap.get("status").toString(), integer);
            if (status >= 1) {
                // 进行总数添加
                if (Integer.parseInt(hashMap.get("status").toString()) == 1){
                    Integer userId = articleSocialMapper.articleUserLikeAdd(Integer.parseInt(hashMap.get("articleId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }else if (Integer.parseInt(hashMap.get("status").toString()) == 0){
                    Integer userId = articleSocialMapper.articleUserLikeSub(Integer.parseInt(hashMap.get("articleId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }
                return true;

            }
        }else{
            Integer integer1 = articleSocialMapper.articleUserLike(hashMap);
            if (integer1 >= 1){
                if (Integer.parseInt(hashMap.get("status").toString()) == 1){
                    Integer userId = articleSocialMapper.articleUserLikeAdd(Integer.parseInt(hashMap.get("articleId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }else if (Integer.parseInt(hashMap.get("status").toString()) == 0){
                    Integer userId = articleSocialMapper.articleUserLikeSub(Integer.parseInt(hashMap.get("articleId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
