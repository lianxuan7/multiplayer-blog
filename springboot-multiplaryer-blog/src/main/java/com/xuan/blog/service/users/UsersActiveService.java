package com.xuan.blog.service.users;


import com.xuan.blog.mapper.users.UsersActiveMapper;
import com.xuan.blog.pojo.users.Users;
import com.xuan.blog.utils.JwtUtils;
import com.xuan.blog.utils.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UsersActiveService {

    @Autowired
    UsersActiveMapper usersActiveMapper;

    public ResultVO userLogin(Users users){
        Users usersPojo = usersActiveMapper.userLogin(users);

        if (usersPojo == null){
            return new ResultVO(500,"用户名密码错误",null);
        }

        Integer integer = usersPojo.getUserId().intValue();
        HashMap<String,String> map = new HashMap<>();
        map.put("token",JwtUtils.createToken(integer));
        map.put("userId",Integer.toString(integer));

        return new ResultVO(200,"登录成功",map);
    }
}
