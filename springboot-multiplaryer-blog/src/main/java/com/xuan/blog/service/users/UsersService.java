package com.xuan.blog.service.users;


import com.xuan.blog.mapper.users.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UsersService {

    @Autowired
    UsersMapper usersMapper;


    public String usersHeadpic(Integer userId) {
        return usersMapper.usersHeadpic(userId);
    }


    public String usersHomeAddress(Integer userId) {
        return usersMapper.usersHomeAddress(userId);
    }


    public Boolean userFollow(HashMap hashMap) {
        Integer integer = usersMapper.usersIsFollow(hashMap);
        if (integer != null) {
            Integer integer1 = usersMapper.usersModifyFollow(hashMap.get("status").toString(), integer,hashMap.get("currentTime").toString());
            if (integer1 >= 1) {
                if (Integer.parseInt(hashMap.get("status").toString()) == 1){
                    Integer userId = usersMapper.userSocialAdd(Integer.parseInt(hashMap.get("passiveFollowId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }else if (Integer.parseInt(hashMap.get("status").toString()) == 0){
                    Integer userId = usersMapper.userSocialSub(Integer.parseInt(hashMap.get("passiveFollowId").toString()));
                    if (userId >= 1){
                        return true;
                    }
                }
            }
        } else {
            Integer integer1 = usersMapper.userFollow(hashMap);
            if (integer1 >= 1) {
                if (integer1 >= 1) {
                    if (Integer.parseInt(hashMap.get("status").toString()) == 1){
                        Integer userId = usersMapper.userSocialAdd(Integer.parseInt(hashMap.get("passiveFollowId").toString()));
                        if (userId >= 1){
                            return true;
                        }
                    }else if (Integer.parseInt(hashMap.get("status").toString()) == 0){
                        Integer userId = usersMapper.userSocialSub(Integer.parseInt(hashMap.get("passiveFollowId").toString()));
                        if (userId >= 1){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

}
