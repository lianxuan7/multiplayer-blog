# 博客系统



## 1. 介绍

> 此项目为多人博客项目，内置小组件。

项目地址： https://gitee.com/xuanxiaoqian/multiplayer-blog



## 2. 数据库设计

### 用户表 users

| 字段          | 类型         | 说明                   |
| ------------- | ------------ | ---------------------- |
| user_id       | bigint       | 用户ID                 |
| user_name     | varchar(50)  | 用户名                 |
| pass_word     | varchar(50)  | 用户密码               |
| email         | varchar(60)  | 邮箱                   |
| nick_name     | varchar(50)  | 昵称                   |
| gender        | varchar(5)   | 性别(1:男 2:女 0:未知) |
| headpic       | varchar(255) | 头像地址               |
| mobile_phone  | varchar(32)  | 手机号                 |
| register_time | varchar(255) | 注册时间 时间戳        |
| summary       | varchar(200) | 自我简介               |
| home_address  | varchar(100) | 自定义的访问路径       |
| qq_number     | varchar(50)  | QQ号                   |
| we_chat       | varchar(50)  | 微信号                 |
| blog_address  | varchar(100) | 自己的博客链接         |



### 文章表 article

| 字段           | 类型         | 说明                |
| -------------- | ------------ | ------------------- |
| article_id     | bigint       | 文章ID              |
| push_date      | varchar(255) | 发布日期 时间戳     |
| modify_date    | varchar(255) | 最后修改日期 时间戳 |
| user_id        | bigint       | 发表用户ID          |
| title          | varchar(150) | 博客标题            |
| like_count     | int          | 点赞数              |
| comment_count  | int          | 评论数              |
| read_count     | int          | 浏览量              |
| is_draft       | varchar(10)  | 是否草稿(0:否1:是)  |
| aricle_summary | varchar(255) | 文章摘要            |
| article_img    | varchar(255) | 文章封面地址(可选)  |
| category_id    | bigint       | 分类ID              |
| tag_id         | bigint       | 标签ID              |



### * 文章草稿箱article_draft

已删除，用article表的is_draft代替

| 字段         | 类型         | 说明                |
| ------------ | ------------ | ------------------- |
| art_draft_id | bigint       | 文章草稿箱id        |
| content_md   | longtext     | 文章markdown内容    |
| user_id      | bigint       | 用户ID              |
| create_time  | varchar(255) | 创建时间 时间戳     |
| modify_date  | varchar(255) | 最后修改日期 时间戳 |





### 文章收藏表article_collect

| 字段           | 类型         | 说明            |
| -------------- | ------------ | --------------- |
| art_collect_id | bigint       | 文章收藏id      |
| article_id     | bigint       | 文章ID          |
| user_id        | bigint       | 收藏用户ID      |
| create_time    | varchar(255) | 创建时间 时间戳 |







### 文章详情表 article_detail

| 字段          | 类型     | 说明             |
| ------------- | -------- | ---------------- |
| art_detail_id | bigint   | 文章详情id       |
| content_md    | longtext | 文章markdown内容 |
| content_html  | longtext | 文章html内容     |
| article_id    | bignit   | 文章ID           |



### 文章分类表 article_category

| 字段            | 类型         | 说明            |
| --------------- | ------------ | --------------- |
| art_category_id | bigint       | 分类ID          |
| category_name   | varchar(46)  | 分类名称        |
| description     | varchar(128) | 分类描述        |
| create_time     | varchar(255) | 创建时间 时间戳 |



### 文章标签表 article_tag

| 字段        | 类型         | 说明            |
| ----------- | ------------ | --------------- |
| art_tag_id  | bigint       | 标签ID          |
| tag_name    | varchar(64)  | 标签名称        |
| description | varchar(128) | 标签描述        |
| create_time | varchar(255) | 创建时间 时间戳 |



### 文章评论表 article_discuss

| 字段           | 类型          | 说明            |
| -------------- | ------------- | --------------- |
| art_discuss_id | bigint        | 评论ID          |
| create_time    | varchar(255)  | 评论日期 时间戳 |
| like_count     | int           | 点赞数          |
| user_id        | bigint        | 发表用户ID      |
| article_id     | bigint        | 评论文章ID      |
| content        | varchar(3072) | 评论内容content |



### 文章点赞表 article_praise

| 字段          | 类型         | 说明                   |
| ------------- | ------------ | ---------------------- |
| art_praise_id | bigint       | 文章点赞ID             |
| article_id    | bigint       | 文章ID                 |
| create_time   | varchar(255) | 文章点赞日期 时间戳    |
| user_id       | bigint       | 文章点赞用户ID         |
|               | tinyint(1)   | 点赞状态 (0取消 1点赞) |



###  文章评论点赞表 article_discuss_praise

| 字段              | 类型         | 说明                    |
| ----------------- | ------------ | ----------------------- |
| art_dis_praise_id | bigint       | 评论点赞ID              |
| discuss_id        | bigint       | 评论ID                  |
| create_time       | varchar(255) | 文章评论点赞日期 时间戳 |
| user_id           | bigint       | 文章评论点赞用户ID      |



### 用户关注、粉丝数、文章数表 user_social 

| 字段           | 类型   | 说明   |
| -------------- | ------ | ------ |
| user_social_id | bigint | 社交ID |
| user_id        | bigint | 用户ID |
| fans_count     | int    | 粉丝数 |
| follow_count   | int    | 关注数 |
| article_count  | int    | 文章数 |





### 用户关注表 user_follow

| 字段                | 类型         | 说明                  |
| ------------------- | ------------ | --------------------- |
| user_follow_id      | bigint       | 关注ID                |
| active_follower_id  | bigint       | 关注者用户ID          |
| passive_follower_id | bigint       | 被关注者用户ID        |
| status              | tinyint(1)   | 关注状态(0取消 1关注) |
| create_time         | varchar(255) | 创建时间 时间戳       |
| update_time         | varchar(255) | 修改时间 时间戳       |





## 3. 技术栈

> 采用前后端分离技术 Vue + SpringBoot

前端技术

| 名字         | 说明               |
| ------------ | ------------------ |
| Vue3         | SPA框架            |
| Vue-Router 4 | 路由               |
| Pinia        | 全局状态管理       |
| Axios        | HTTP请求           |
| Md-editor-v3 | Markdown编辑器     |
| Typescript   | JavaScript类型提示 |
| Vite         | 前端开发与构建工具 |
| Nprogress    | 页面加载进度条     |
| Scss         | css扩展框架        |
| Element-Plus | Vue3组件库         |
| Prettierrc   | 格式化代码         |







后端技术

| 名字        | 说明      |
| ----------- | --------- |
| SpringBoot  | MVC框架   |
| MyBatis     | ORM框架   |
| Jwt         | token生成 |
| Lombok      | 简化代码  |
| Mysql 5.0.5 | 数据库    |




