# 页面流程说明书



## 主页

### 1. 中性

~~~mysql
# mysql语句
select u.nick_name, 
		a.article_id,a.push_date,a.title,a.article_img,a.article_summary,a.read_count,a.like_count,a.comment_count,

t.tag_name

from 
	article a 
join 
	users u 
on 
	a.user_id = u.user_id
join 
	article_tag t
on a.tag_id = art_tag_id;



| nick_name | article_id | push_date | title | article_img | article_summary | read_count | like_count | comment_count | tag_name |
~~~



~~~json
// 文章列表
{
    articleId:1,
    pushDate:8484694849,
    TagName:'Vue.js',
    title:'我是文章标题',
    articleImg?:'xuanxiaoqian.com/article,jpg',
    nickName:'轩小浅',
    articleSummary:'我是文章的摘要鸭',
    readCount:999,
    likeCount:199,
    commentCount:100
}
~~~





### 2. 未登录

显示登录注册按钮，屏蔽点赞功能





### 3. 已登录

~~~json
// 头部数据
{
    userId:1,
    userName:'轩小浅',
    headpic:'xuanxiaoqian.com/xuan1.jpg'
}
~~~







## 文章详情页

### 1. 中性

~~~json
// 文章内容
{
    articleId:1,
    pushDate:8484694849,
    TagName:'Vue.js',
    title:'我是文章标题',
    articleImg?:'xuanxiaoqian.com/article,jpg',
    nickName:'轩小浅',
    articleSummary:'我是文章的摘要鸭',
    readCount:999,
    likeCount:199,
    commentCount:100,
    contentMd:'xxxx'
}
~~~





### 2. 未登录

屏蔽点赞、关注、评论功能



### 3. 已登录

...







## 用户主页

~~~mysql
select u.user_id,u.nick_name,u.headpic,u.summary,
		s.fans_count,s.follow_count,s.article_count,
		f.user_follow_id
from 
	users u
join
	user_social s
on u.user_id = s.user_id and u.home_address = 'xuanxiaoqian'
left join
	user_follow f
on f.passive_follow_id = u.user_id and active_follow_id= 1;
~~~

**左连接查询**









## 文章卡片

~~~mysql
select u.nick_name,
		a.article_id,a.push_date,a.title,a.article_img,a.article_summary,a.read_count,a.like_count,a.comment_count,
		t.tag_name,
		
		ifnull(p.art_praise_id,0) artPariseId

    from
        article a
    join
        users u
    on
        a.user_id = u.user_id
    join
        article_tag t
    on a.tag_id = art_tag_id
    left join
    	article_praise p
    on p.article_id = a.article_id and p.user_id = 1 and p.status = 1;
~~~





## 文章内容

~~~mysql
select
            u.user_id userId,u.nick_name nickName,u.headpic,

            a.article_id articleId,a.push_date pushDate,a.title,a.like_count likeCount,a.comment_count commentCount,
            a.read_count readCount,a.article_img articleImg,

            d.content_md contentMd,

		    ifnull(f.user_follow_id,0) userFollowId

        from
            article a
        join
            users u
        on
            a.user_id = u.user_id and a.article_id = #{articleId}
        left join
            article_detail d
        on
            a.article_id = d.article_id
        left join
            user_follow f
        on
            f.passive_follow_id = a.user_id and active_follow_id= #{userId};
~~~







## 文章评论

~~~mysql
select 
	* 
from 
	article_discuss a
join
	users u
where a.article_id = 3 and u.user_id = a.user_id;
~~~









## 新增文章

~~~mysql
insert into article(push_date pushDate, modify_date modifyDate,user_id userId,title,
                    like_count lickCount,comment_count commentCount,
                    read_count readCount,top_flag topFlag, 
    				article_summary articleSummary,article_img articleImg,
                    category_id category_id,tag_id tagId)
                    
             value(1,1,1,1,1,1,1,1,1,1,1,1);
~~~

~~~mysql
insert into article(push_date, modify_date,user_id,title,
                    like_count,comment_count,
                    read_count,top_flag, 
    				article_summary,article_img,
                    category_id,tag_id)
                    
             value(1,1,1,1,1,1,1,1,1,1,1,1);
~~~

