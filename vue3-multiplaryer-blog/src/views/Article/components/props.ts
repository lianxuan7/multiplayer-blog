export interface articleCardProps {
  articleId: number // 文章ID
  pushDate: string // 发布时间
  nickName: string // 发表用户昵称
  tagName: string // 标签

  title: string // 文章题目
  articleSummary: string // 文章摘要
  likeCount: number // 文章点赞数
  commentCount: number // 文章评论数
  readCount: number // 文章浏览量
  articleImg?: string // 文章封面

  artPariseId?: number  // 当前用户是否点赞 0未点赞 其他数字为点赞表id则为点赞
}

export interface articleContentPorps {
  articleId: number // 文章ID
  pushDate: string // 发布时间

  userId: number // 用户ID
  nickName: string // 发表用户昵称
  headpic: string // 用户头像

  title: string // 文章题目
  likeCount: number // 文章点赞数
  commentCount: number // 文章评论数
  readCount: number // 文章浏览量
  articleImg?: string // 文章封面

  contentMd: string //文章markdown内容

  userFollowId:number // 是否关注
}
