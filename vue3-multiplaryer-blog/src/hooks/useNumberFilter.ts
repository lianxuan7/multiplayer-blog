// 时间戳距离当前时间多久
export const activeDate = (oldDate: any): string => {
  var curTimestamp = Date.parse(new Date().toString()) //当前时间戳

  let timeDiff = (curTimestamp - Number(oldDate)) / 1000

  if (timeDiff < 60) {
    return '刚刚'
  } else if (timeDiff < 3600) {
    return Math.ceil(timeDiff / 60) + '分钟前'
  } else if (timeDiff < 86400) {
    return Math.ceil(timeDiff / 3600) + '小时前'
  } else if (timeDiff < 2592000) {
    return Math.ceil(timeDiff / 86400) + '天前'
  } else if (timeDiff < 31104000) {
    return Math.ceil(timeDiff / 2592000) + '月前'
  } else {
    return Math.ceil(timeDiff / 31104000) + '年前'
  }
}

// 转为标准时间
export const standardDate = (oldDate: number) => {
  var time = new Date(oldDate)

  function timeAdd0(str: any) {
    if (str < 10) {
      str = '0' + str
    }
    return str
  }
  var y = time.getFullYear() // 年
  var m = time.getMonth() + 1 // 月
  var d = time.getDate() // 日
  var h = time.getHours() // 时
  var mm = time.getMinutes() // 分
  var s = time.getSeconds() // 秒
  return (
    y + '-' + timeAdd0(m) + '-' + timeAdd0(d) + ' ' + timeAdd0(h) + ':' + timeAdd0(mm)
    // ':' +
    // timeAdd0(s)
  )
}

// 数字量转为 W
export const transW = (count: number) => {
  if (count >= 10000) {
    return String(Math.ceil(count) / 10000).substring(0, 3) + 'w'
  }

  return count
}
