import { rejects } from 'assert';
import { articleUserLikeAdd } from '/@/api/article/ArticleSocial'

let userId = localStorage.getItem('userId')

export default function () {
  const EventArticleLike = async (articleId: number, status: boolean) => {
    return new Promise((resolve) => {
      if (userId == null || userId == '') {
        resolve(false)
        return false
      }
      articleUserLikeAdd({
        articleId: articleId,
        userId: userId,
        status: status ? 0 : 1,
        currentTime: Date.parse(new Date().toString())
      }).then((res) => {
        if (res.data) {
          resolve(true)
        }
      })
    })
  }

  return { EventArticleLike }
}
