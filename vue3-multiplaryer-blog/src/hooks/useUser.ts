import { userFollow } from '../api/users/usersActive'

let userId = localStorage.getItem('userId')

export default function () {

  const  EventFollow = async (passiveFollowId: number, status: boolean) => {

    return new Promise((resolve)=>{
      if (userId == null || userId == '') {
        resolve(false)
        return false
      }
      userFollow({
        activeFollowId: userId,
        passiveFollowId: passiveFollowId,
        status: status ? 0 : 1,
        currentTime: Date.parse(new Date().toString())
      }).then((res) => {
        if (res.data) {
          resolve(true)
        }
      })

    })
  }


  return { EventFollow }
}
