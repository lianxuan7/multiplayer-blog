import { defineComponent, ref, toRef, watch } from 'vue'

import { makeArrayProp, makeBooleanProp, makeNumberProp, makeStringProp } from '../props';

import './index.scss'

type navLists = {
    name: string,
}

const headProps = {
    navLists: makeArrayProp<navLists>(),
    isPlace: makeBooleanProp<boolean>(true),
    navLiStyle: makeStringProp<string>(''),
    modelValue: makeNumberProp<number>(0),
}



export default defineComponent({
    name: 'VuaHead',
    props: headProps,
    emits: ['iconClick', 'navClick', 'update:modelValue'],
    setup(props, { slots, emit }) {

        // const liActive = ref(props.modelValue);  // 用toRef解决了响应式失败，简洁了4行代码
        const liActive = toRef(props, 'modelValue');

        // watch(() => props.modelValue, (activeIndex) => {
        //     liActive.value = activeIndex;
        // })

        const onClickIcon = () => emit('iconClick')

        const onClickNav = (name: string, index: number) => {
            // liActive.value = index;
            emit('update:modelValue', index)
            emit('navClick', name, index)
        }

        const rendeNavUl = () => {
            const Items = props.navLists.map((item, index) => (
                <li style={props.navLiStyle}>
                    <span class={liActive.value == index ? 'vua-head__main-nav--active' : ''}
                        onClick={() => onClickNav(item.name as string, index)}>
                        {item.name}
                    </span>
                </li >
            ))
            return (<ul>{Items}</ul>)
        }

        return () => {
            return (
                <>
                    <div class="vua-head">
                        <div class="vua-head__main">
                            <div class="vua-head__main-icon" onClick={onClickIcon}>
                                {slots.icon ? slots.icon() : ''}
                            </div>

                            <div class="vua-head__main-nav">
                                {rendeNavUl()}
                            </div>

                            <div class="vua-head__main-middle">
                                {slots.middle ? slots.middle() : ''}
                            </div>

                            <div class="vua-head__main-right">
                                <div style="height: 100%;width: 100%; display: flex;justify-content: flex-end; align-items: center;">
                                    {slots.right ? slots.right() : ''}
                                </div>
                            </div>
                        </div>

                    </div>

                    {props.isPlace == true ? (<div class="vua-stea"></div>) : ''}
                </>
            )
        }
    }
})