import { defineComponent, Teleport } from 'vue'
import { makeBooleanProp } from '../props'
import './index.scss'

const modalProps = {
    modelValue: makeBooleanProp<boolean>(false),
}

export default defineComponent({
    name: 'VuaModal',
    emits: ['update:modelValue'],
    props: modalProps,
    setup(props, { emit, slots }) {
        const rendeModal = () => {
            if (props.modelValue) {
                return (
                    <div class="vua-modal">
                        {slots.default ? slots.default() : ''}
                    </div>
                )
            }
        }

        return () => {
            return (
                <Teleport to="body">
                    {rendeModal()}
                </Teleport>
            )
        }
    }
})