import http from '/@/utils/http'

export const articleAdd = (params:any) => http.post('/article/add',params)

export const articleAddDraft = (params:any) => http.post('/article/addDraft',params)

export const articleAddDiscuss = (params:any) => http.post('/article/addDiscuss',params)

export const articleDelDraft = (params:any) => http.post('/article/delDraft',params)

export const articleUpdate = (params:any) => http.post('/article/updateArticle',params)



