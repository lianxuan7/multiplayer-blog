import http from '/@/utils/http'

export const articleContent = (params:any) => http.post('/article/content',params)

export const articleDiscuss = (params:any) => http.post('/article/discuss',params)

export const draftContent = (params:any) => http.post('/article/draftContent',params)



