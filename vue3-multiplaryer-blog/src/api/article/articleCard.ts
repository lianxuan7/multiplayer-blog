import http from '/@/utils/http'

export const articleCard = (parent: any) => http.post('/article/card', parent)

export const articleUserCard = (parent: any) => http.post('/article/card/user', parent)

export const articleUserDraftCard = (parent: any) => http.post('/article/card/draft', parent)

export const searchArticle = (parent: any) => http.post('/article/search', parent)
