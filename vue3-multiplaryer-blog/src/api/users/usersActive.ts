import http from '/@/utils/http'

export const userLogin = (params: any) => http.post('/users/userlogin', params)

export const userHeadpic = () => http.post('/users/headpic')

export const userHomeAddress = (params: any) => http.post('/users/homeAddress', params)

export const userFollow = (params: any) => http.post('/users/follow', params)

