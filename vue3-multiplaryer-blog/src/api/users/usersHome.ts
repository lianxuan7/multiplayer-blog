import http from '/@/utils/http'


export const userHomeMeta = (params:any) => http.post('/users/homeMeta',params)

export const userHomeSettingsMeta = (params:any) => http.post('/users/homeSettingsMeta',params)

export const userHomeSettingsUpdate = (params:any) => http.post('/users/homeSettingsUpdate',params)
