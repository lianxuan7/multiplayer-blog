import http from '/@/utils/http'

export const imagesUpload = (form: FormData) => {
  return http.post('/utils/imagesUpload', form, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}
